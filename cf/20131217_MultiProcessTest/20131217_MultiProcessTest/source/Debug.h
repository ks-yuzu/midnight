#pragma once

#include "Input.h"

class Game;
class Console;
class OutputConsole;
class Dungeon;

class Debug : private InputMgr
{
private:
	Game *pGame;

	Console *inCon;				bool fInConsole;
	OutputConsole *dunConsole;	bool fDun;
	Dungeon *dun;

	void UpdateOutputFlag();
	void MyAllocConsole();
	void GetPointer();
	void PutDebugData();

public:
	Debug();
	~Debug();
	void DebugMgr(Game*);

};