#pragma once

class Rand
{
public:
	static bool Initialize();
	static int GetRand(int min, int max);
	static double GetRand(double min, double max);

};