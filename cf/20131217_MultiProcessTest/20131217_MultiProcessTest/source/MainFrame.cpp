#include "DxLib.h"

#include "Game.h"
#include "Input.h"
#include "Fps.h"

namespace {

class GameFrame : private Game
{
public:
	int GameMain()							//ゲーム内容以外のシステム処理（カウンタ・デバッグ関連etc）
	{
		if( !Initialize() ) return -1;

		while( !ProcessMessage() && !ClearDrawScreen() && pInput->UpdateKeyState() )
		{
			if( ConfirmExit() ) break;		//終了確認 → 戻り値true

			pFps->Update();

			//メイン処理
			if( !ProcessFrame() ) break;	//異常発生 → 戻り値false

			ScreenFlip();
			pFps->Wait();					//FPS調整用のウェイト

		}

		Terminalize();
		return 0;
	}

};


} //unnamed namespace


int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpCmdLine, int nCmdShow )
{
	GameFrame gameObj;
	
	return gameObj.GameMain();				//GameFrameクラスに処理を引き継ぐ
}