#include "OutputConsole.h"

#include <time.h>
#include <iomanip>

class LogConsole : public OutputConsole
{
private:
	bool fTime;

public:
	template<class T> LogConsole& operator<<(T val)
	{
		if (fTime && val != endline)
		{
			time_t longTime;
			struct tm nowTime;

			time(&longTime);
			localtime_s(&nowTime, &longTime);

			stringstream strtime;
			strtime << setw(2) << right << nowTime.tm_hour << ":";
			strtime << setw(2) << right << nowTime.tm_min << ":";
			strtime << setw(2) << right << nowTime.tm_sec;
			strtime << "  ";

			Print(strtime.str());
			fTime = false;
		}

		stringstream buf;
		buf << val << "|";

		if (val == endline)	fTime = true;

		Print(buf.str());

		return *this;
	}


};