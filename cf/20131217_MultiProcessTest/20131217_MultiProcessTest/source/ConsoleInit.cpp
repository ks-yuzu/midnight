#pragma warning (disable : 4996)

#include "Console.h"
#include <iostream>
#include "DxLib.h"
#include <Winuser.h>

#define CONSOLE_WIDTH 670
#define CONSOLE_HEIGHT 500
#define TASKBER_HEIGHT 21


Console::Console()
{
	::AllocConsole();					// コンソール表示
	freopen( "CONOUT$", "w", stdout );	// 標準出力を割り当て
	freopen( "CONIN$", "r", stdin  );	// 標準入力を割り当て

	//コンソールの初期位置設定
	int posX = GetSystemMetrics( SM_CXSCREEN ) - GetSystemMetrics( SM_CXFULLSCREEN );
    int posY = GetSystemMetrics( SM_CYFULLSCREEN ) - CONSOLE_HEIGHT;
	MoveWindow(GetConsoleWindow(), posX , posY, CONSOLE_WIDTH, CONSOLE_HEIGHT , TRUE);

	SetConsoleTitle("DebugConsle");
	cout << "welcome to console\n\n" ;
	SetForegroundWindow(GetMainWindowHandle());		//メインウィンドウをアクティブに

	SetConsoleCtrlHandler(ConsoleHandlerRoutine, TRUE);								//割り込みハンドラの登録
	RemoveMenu( GetSystemMenu(GetConsoleWindow(), FALSE) , SC_CLOSE, MF_BYCOMMAND);	//×ボタン無効化

}



Console::~Console()
{
	::FreeConsole();
}



BOOL WINAPI Console::ConsoleHandlerRoutine(DWORD dwCtrlType)	//コンソールウィンドウのハンドラ関数
{
	if (dwCtrlType == CTRL_C_EVENT || CTRL_BREAK_EVENT)
		return TRUE;
	else
		return FALSE;
}
