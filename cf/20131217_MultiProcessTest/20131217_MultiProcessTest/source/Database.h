#include <vector>

#include "Player.h"
#include "Enemy.h"

using namespace std;

//singleton
class Database
{
private:
	Database();
	Database(const Database&);
	Database& operator=(const Database&);

	void InitPlayer();

public:
	Player player;
	vector<Enemy> enemy;

	static Database* GetInstance();				//インスタンスはここの静的ローカル変数

};