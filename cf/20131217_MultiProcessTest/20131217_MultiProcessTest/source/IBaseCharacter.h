#pragma once

class IBaseCharacter
{
public:
	virtual ~IBaseCharacter();

	virtual void SetMoveVector() = 0;
	virtual	bool IsMoving() = 0;
	virtual void Move() = 0;

	virtual void Draw() = 0;

};