#include <vector>
#include <list>

#include "BaseScene.h"
#include "ISceneChanger.h"

#include "Chip.h"
#include "Player.h"
#include "Input.h"

class Enemy;
class Debug;

class Dungeon : public BaseScene, private InputMgr
{
	friend class Debug;

private:
	int cntDungeon;

//	Chip map[10][7];						//スクロールなし（開発用）
	vector< vector<Chip> > map;				//map( 10, vector<Chip>(7) )

	Player player;
	list<Enemy *> enemyList;
	list<Enemy *>::iterator itEnemy;

	void MakeMap();
	
public:
	Dungeon(ISceneChanger *);				//開発用 -> 実際はエリア情報などを取得
	virtual ~Dungeon();

	bool Process() override;
	bool Draw() override;

	bool CanEnter(int, int);
};

//部屋の座標を保存しておく
