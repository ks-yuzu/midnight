#include "Player.h"
#include "DxLib.h"

#include "Database.h"
#include "Dungeon.h"

Player::Player(){}
Player::Player(Dungeon *pd)
{
	Database *db = Database::GetInstance();
	*this = db->player;

	pDun = pd;

	pos.x = 96 * 1;
	pos.y = 96 * 1;


}

Player::~Player(){}


void Player::SetMoveVector()
{
	if( pos.x % 96 || pos.y % 96 )	return;	//マス上でなければ値を維持

	int r = IsRight();	int l = IsLeft();
	int d = IsDown();	int u = IsUp();		//bool -> int の変換は 0 or 1 が保障されている

	int nextX = (pos.x / 96) + r - l;
	int nextY = (pos.y / 96) + d - u;

	if( pDun->CanEnter(nextX, nextY) )
	{
		moveVector.x = 8 * (r - l);
		moveVector.y = 8 * (d - u);
	}
	else
	{
		moveVector.x = 0;
		moveVector.y = 0;
	}

	//SetImage
	if( l + r + u + d != 0 )										//入力があれば
		idxImg = 12 + (u * -9) + (d * 9) + (l * -3) + (r * 3) + 1;	//対応した向きの画像をセット

}


void Player::Draw()
{
	DrawGraph(pos.x, pos.y, hImg[idxImg], true);
}

