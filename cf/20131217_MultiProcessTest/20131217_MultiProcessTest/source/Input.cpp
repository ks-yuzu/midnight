#include "Input.h"
#include "DxLib.h"

unsigned int InputMgr::key[256];

bool InputMgr::UpdateKeyState()
{
	char buf[256];

	if( GetHitKeyStateAll(buf) == -1 )
		return false;

	for(int i = 0; i < 256; i++)
		if(buf[i] == 1)	key[i]++;
		else			key[i] = 0;

	return true;
}

unsigned int InputMgr::GetKey(int keyId) { return key[keyId]; }

bool InputMgr::IsUp()		{ return !!key[KEY_INPUT_UP];		}	//2回論理否定することで警告が出ないようにしている。
bool InputMgr::IsDown()		{ return !!key[KEY_INPUT_DOWN];		}
bool InputMgr::IsRight()	{ return !!key[KEY_INPUT_RIGHT];	}
bool InputMgr::IsLeft()		{ return !!key[KEY_INPUT_LEFT];		}

bool InputMgr::IsEnter()	{ return !!key[KEY_INPUT_Z] || key[KEY_INPUT_RETURN]; }
bool InputMgr::IsCancel()	{ return !!key[KEY_INPUT_X] || key[KEY_INPUT_ESCAPE]; }