#include <algorithm>
#include "Dungeon.h"
#include "Enemy.h"

#include "LogOutput.h"

#define MAP_WIDTH 10
#define MAP_HEIGHT 7


using namespace std;

Dungeon::Dungeon(ISceneChanger *changer)
:cntDungeon(0), map(10, vector<Chip>(7)), player(this)
{
	sceneChanger = changer;
	thisScene = eScene_DUNGEON;

	Log << "[start] dungeon" << endline;

	MakeMap();

	enemyList.push_back(new Enemy(1));
	enemyList.push_back(new Enemy(2));

}

Dungeon::~Dungeon()
{
	Log << "[ end ] dungeon" << endline;
}


void Dungeon::MakeMap()
{
	const int tempmap[7][10] = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 1, 0, 0, 1, 1, 1, 0, 1, 1 },
		{ 1, 1, 0, 1, 1, 0, 0, 0, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	};

	for( int i = 0; i < MAP_WIDTH; i++ )
		for( int j = 0; j < MAP_HEIGHT; j++ )
			map[i][j].SetEnterFlag( !tempmap[j][i] );

}


bool Dungeon::Process()
{
	cntDungeon++;

	bool fActed = true;
	player.SetMoveVector();
	if( player.IsMoving() )			player.Move();
//	else if ( player.IsActing() )	???
	else							fActed = false;

	if( fActed )
		for_each(enemyList.begin(), enemyList.end(), [](Enemy *ene){ ene->Act(); });		//全エネミー行動

	this->Draw();

	return true;
}

bool Dungeon::Draw()
{
	for( int i = 0; i < MAP_WIDTH; i++ )
		for( int j = 0; j < MAP_HEIGHT; j++ )
			map[i][j].Draw(i, j);

	player.Draw();
	//enemy draw

	return true;
}

bool Dungeon::CanEnter(int x, int y){ return map[x][y].GetEnterFlag(); }