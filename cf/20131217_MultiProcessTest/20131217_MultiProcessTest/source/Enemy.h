#include "BaseCharacter.h"
#include "Rand.h"

class Enemy : public BaseCharacter, private Rand
{
private:
	void SetMoveVector() override;

public:
	Enemy();
	Enemy(int);

	void Act();
	void Draw() override;
};
