#include "BaseCharacter.h"

const position zeroVector = { 0, 0 };

BaseCharacter::BaseCharacter()
:moveVector(0, 0)
{}

BaseCharacter::~BaseCharacter(){}

void BaseCharacter::Move()
{
	pos += moveVector;
}

bool BaseCharacter::IsMoving()
{
	return moveVector != zeroVector;
}
