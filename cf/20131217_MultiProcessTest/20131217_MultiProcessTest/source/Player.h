#pragma once

#include "BaseCharacter.h"
#include "Input.h"

class Database;
class Debug;

class Player : public BaseCharacter, private InputMgr
{
	friend class Database;
	friend class Debug;

private:

public:
	Player();
	Player(Dungeon *);		//�ʏ�g�p
	virtual ~Player();

	void SetMoveVector() override;

	void Draw() override;
};