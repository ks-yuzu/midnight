#include "OutputConsole.h"
#include <process.h>
#include <iostream>


char OutputConsole::consoleFile[] = "20131221_OutputConsole.exe";

OutputConsole::OutputConsole()
	:hParenProcess( GetCurrentProcess() ),
	hChildProcess(NULL),
	readPipe(nullptr),
	writePipe(nullptr)
{

	HANDLE readTemp;
	// パイプを作成（両ハンドルとも子プロセスへ継承不可）
	if (!CreatePipe(&readTemp, &writePipe, NULL, 0))
		cout << "CreatePipe";

	// 読込ハンドルを複製（子プロセスへ継承可能な権限の読込ハンドルを作成）
	if ( !DuplicateHandle( hParenProcess, readTemp, hParenProcess, &readPipe, 0, TRUE, DUPLICATE_SAME_ACCESS) )
	{
		cout << "DuplicateHandle";
		if (!CloseHandle(readTemp))
			cout << "CloseHandle(readTemp)";
	}

	// 複製元のハンドルは使わないのでクローズ
	if (!CloseHandle(readTemp))
		cout << "CloseHandle(readTemp)";

	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.hStdInput = readPipe;
	si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdError = GetStdHandle(STD_ERROR_HANDLE);

	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOWNOACTIVATE;


	if (si.hStdOutput == INVALID_HANDLE_VALUE)
		cout << "GetStdHandle(STD_OUTPUT_HANDLE)";

	if (si.hStdError == INVALID_HANDLE_VALUE) 
		cout << "GetStdHandle(STD_ERROR_HANDLE)";

	Process();
}


OutputConsole::~OutputConsole()
{
	*this << "!exit";
	CloseHandle(writePipe);
	CloseHandle(hChildProcess);
}


void OutputConsole::Print(string str)
{
	DWORD dummy;
	WriteFile(writePipe, str.c_str(), strlen(str.c_str()), &dummy, NULL);
}



//=======================================
//           子プロセス側
//=======================================

bool OutputConsole::Process()
{
	hProcThread = (HANDLE)_beginthreadex(NULL, 0, LaunchThread, this, 0, NULL);


	if (hProcThread != nullptr)
		CloseHandle(hProcThread);

	return true;
}


// staticでないメンバ関数をマルチスレッドにするために、この関数をかませる
unsigned int __stdcall OutputConsole::LaunchThread(void *ptr)
{
	return reinterpret_cast<OutputConsole*>(ptr)->TProcess();
}


unsigned int __stdcall OutputConsole::TProcess()
{
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	if (!CreateProcess(NULL, consoleFile, NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi))
		cout << "error!";

	// Set child process handle to cause threads to exit.
	hChildProcess = pi.hProcess;

	// Close any unnecessary handles.
	CloseHandle(pi.hThread);

	// Close pipe handles (do not continue to modify the parent).
	// You need to make sure that no handles to the write end of the
	// output pipe are maintained in this process or else the pipe will
	// not close when the child process exits and the ReadFile will hang.
	CloseHandle(readPipe);
	readPipe = nullptr;

	return 0;

}
