//#include <math.h>
//#include "Keyboard.h"

class Fps
{	
	int startTime;						//測定開始時刻
	int count;							//カウンタ
	double fps;							//fps
	static const int sampleNum = 60;	//平均を取るサンプル数
	static const int FPS = 60;			//設定したFPS

public:
	Fps();
	bool Update();
	void Draw();
	void Wait();
};