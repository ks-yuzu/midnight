#include <stack>
#include "ISceneChanger.h"

class BaseScene;
class Debug;

class SceneMgr : public ISceneChanger
{
	friend class Debug;

public:
	SceneMgr();
	virtual ~SceneMgr();
	bool Update();

private:
	//Variable
	std::stack<BaseScene *> scene;

	eScene nextScene;
	bool fScenePop;

	// Function
	void SetScene(eScene, bool) override;

};