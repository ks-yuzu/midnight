#include "Database.h"
#include "DxLib.h"

//Database& Database::operator=(const Database& rhs){};

Database::Database()
{
	InitPlayer();
}

void Database::InitPlayer()
{
	int temp[24];
	LoadDivGraph("database/character/player.png", 24, 6, 4, 96, 96, temp);
	player.idxImg = 22;

	//sort
	player.hImg[0] = temp[15];		player.hImg[1] = temp[16];		player.hImg[2] = temp[17];
	player.hImg[3] = temp[18];		player.hImg[4] = temp[19];		player.hImg[5] = temp[20];
	player.hImg[6] = temp[21];		player.hImg[7] = temp[22];		player.hImg[8] = temp[23];
	player.hImg[9] = temp[6];		player.hImg[10] = temp[7];		player.hImg[11] = temp[8];
	player.hImg[12] = -1;			player.hImg[13] = -1;			player.hImg[14] = -1;
	player.hImg[15] = temp[12];		player.hImg[16] = temp[13];		player.hImg[17] = temp[14];
	player.hImg[18] = temp[3];		player.hImg[19] = temp[4];		player.hImg[20] = temp[5];
	player.hImg[21] = temp[0];		player.hImg[22] = temp[1];		player.hImg[23] = temp[2];
	player.hImg[24] = temp[9];		player.hImg[25] = temp[10];		player.hImg[26] = temp[11];

	player.name = "[��l��]";
	player.hp   = 100;

}


Database* Database::GetInstance()
{
	static Database instance;
	return &instance;
}