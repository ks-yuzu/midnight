#include "Fps.h"
#include "DxLib.h"
#include <sstream>
#include <iomanip>

Fps::Fps()
  :startTime(0), count(0), fps(0)
{}


bool Fps::Update()
{
	if( count == 0 )			// 1フレーム目なら時刻を記憶
		startTime = GetNowCount();

	if( count == sampleNum )	//60フレーム目なら平均を計算
	{
		int now = GetNowCount();
		fps = 1000. * sampleNum / (now - startTime);

		count = 0;
		startTime = now;
	}
	
	count++;

	Draw();

	return true;
}

void Fps::Draw()
{
	using namespace std;

	ostringstream os;
	os << "20120822_GameModel  FPS:" << fixed << setprecision(2) << fps;
	SetMainWindowText( os.str().c_str() );
}

void Fps::Wait()
{
	int tookTime = GetNowCount() - startTime;		//経過時間
	int waitTime = count * 1000 / FPS - tookTime;	//待機時間
	if( waitTime > 0 )
		Sleep(waitTime);
}