#pragma once

class InputMgr;
class Console;
class Fps;
class OutputConsole;
class SceneMgr;

class Debug;

//=========================================
// 全体制御クラス（継承して使用）
//=========================================
class Game
{
	friend class Debug;

protected:
	Game();
	virtual ~Game();

	bool Initialize() ;		//処理結果チェックのため、専用の関数とした
	void Terminalize();

	bool ProcessFrame();

	bool ConfirmExit();

	InputMgr *pInput;
	Fps *pFps;

private:
	SceneMgr *pSceneMgr;
	//Sound *pSoundMgr;

	Debug *pDebug;
};