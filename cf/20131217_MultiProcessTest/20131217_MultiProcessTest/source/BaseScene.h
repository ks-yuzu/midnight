#pragma once

#include "Game.h"

class ISceneChanger;
class Debug;

class BaseScene
{
	friend class Debug;

protected:
	enum eScene thisScene;
	ISceneChanger *sceneChanger;

public:
	virtual ~BaseScene() = 0;

	virtual bool Process() = 0;
	virtual bool Draw() = 0;
};