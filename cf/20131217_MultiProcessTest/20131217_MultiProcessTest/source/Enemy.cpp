#include "Enemy.h"
#include "LogOutput.h"

Enemy::Enemy(int num)
{
	string buf("Enemy");
	buf += (char)(num + '0');

	name = buf;
	hp = 100;
	pos.x = 96 * 1;
	pos.y = 96 * 3;
}


void Enemy::Act()
{
	SetMoveVector();
	Move();
}

void Enemy::SetMoveVector()
{
	if( pos.x % 96 || pos.y % 96 )	return;	//マス上でなければ値を維持

	moveVector.x = 8 * GetRand(-1, 1);
	moveVector.y = 8 * GetRand(-1, 1);

	Log << name << " moved." << endline;
}

void Enemy::Draw()
{

}