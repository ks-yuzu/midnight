#pragma once

enum eScene
{
	eScene_NONE, eScene_TITLE, eScene_DUNGEON, eScene_CITY, eScene_GAMEOVER
};

class ISceneChanger
{

public:
	virtual void SetScene(eScene, bool) = 0;

};