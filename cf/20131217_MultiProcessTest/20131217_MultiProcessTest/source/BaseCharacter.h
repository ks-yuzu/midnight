#pragma once

#include <string>
#include "IBaseCharacter.h"

using namespace std;

class Dungeon;
class Debug;

struct position
{
	int x, y;

	position(){}
	position(int inx, int iny) :x(inx), y(iny){};

	position& operator+=(const position& obj)
	{
		x += obj.x;		y += obj.y;
		return *this;
	}

	bool operator==(const position& obj)
	{
		return x == obj.x && y == obj.y;
	}

	bool operator!=(const position& obj)
	{
		return x != obj.x || y != obj.y;
	}
};

extern const position zeroVector;


class BaseCharacter : public IBaseCharacter
{
	friend class Debug;

protected:
	Dungeon *pDun;

	string name;
	position pos;
	position moveVector;

	int hp;

	int hImg[27];
	int idxImg;

public:
	BaseCharacter();
	virtual ~BaseCharacter();

	bool IsMoving() override;
	void Move() override;

};