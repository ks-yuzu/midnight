#include <iostream>
#include "Title.h"

#include "LogOutput.h"

using namespace std;

Title::Title(ISceneChanger *changer)
{
	sceneChanger = changer;
	thisScene = eScene_TITLE;
	Log << "[start] title" << endline;
}

Title::~Title()
{
	Log << "[ end ] title" << endline;
}


bool Title::Process()
{
	if( IsEnter() )		// Enterが押下されたら
		sceneChanger->SetScene(eScene_DUNGEON, true);
	return true;
}

bool Title::Draw()
{
	return true;
}