#pragma once

#include <Windows.h>

//#include <iostream>
//#include <string>
//#include "DxLib.h"

using namespace std;


//コンソールのハンドラ関数プロトタイプ
//BOOL WINAPI C_HandlerRoutine(DWORD);


//================================================
// デバッグ用コンソール制御クラス
//================================================
class Console
{
	private:
		//int f_console_exit;						//ConsoleMgrの戻り値。コマンドexit が入力されたら1が返される
		//void OutPutKeyCommand();				//「メインウィンドウから使うパラメータ出力コマンド」用プロシージャ

		//string str;								//入力コマンド保存用バッファ
		//int GetCommand(string*);				//cin処理部。 |* ﾟーﾟ)コマンド取りに行きますよー
		//int CheckCommand();						//バッファからコマンドを読み取る

	public:
		Console();								//コンストラクタ
		~Console();								//デストラクタ
		bool ConsoleMgr();						//Consoleクラスの管理してます(｀・ω・´)ｷﾘｯ
		//int CheckFlagCin();

		//static UINT CinWorkerThread(LPVOID);	//cin用WT。暗黙のthisポインタが指すインスタンスが特定化されないためstatic必須
		static BOOL WINAPI ConsoleHandlerRoutine(DWORD dwCtrlType);	//メンバ関数ポインタの都合で？static必須
};