#pragma once

#include "KeyId.h"

//================================================
// 入力制御クラス（ゲームパッド未対応）
//================================================
class InputMgr
{
private:
	static unsigned int key[256];		//キー押下フレーム数

public:
	bool UpdateKeyState();		//Key[256]の更新
	static unsigned int GetKey(int);

	static bool IsUp();
	static bool IsDown();
	static bool IsRight();
	static bool IsLeft();

	static bool IsEnter();
	static bool IsCancel();

};