#include "BaseScene.h"
#include "Input.h"
#include "ISceneChanger.h"

class Title : public BaseScene, private InputMgr
{
private:

public:
	Title(ISceneChanger *);
	virtual ~Title();

	bool Process() override;
	bool Draw() override;
};