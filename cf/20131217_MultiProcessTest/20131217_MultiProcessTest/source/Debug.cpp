//#include <vector>

#include "Debug.h"

#include "Game.h"
#include "SceneMgr.h"
#include "Dungeon.h"
#include "Enemy.h"
#include "Console.h"
#include "OutputConsole.h"

#define DEL(ptr) {delete ptr; ptr = nullptr;}

Debug::Debug()
:dunConsole(nullptr), fDun(false), dun(nullptr)
, inCon(nullptr), fInConsole(false)
{}

Debug::~Debug()
{
	if (dunConsole) delete dunConsole;
	if (inCon)		delete inCon;
}

void Debug::DebugMgr(Game *pGame)
{
	this->pGame = pGame;

	UpdateOutputFlag();
	MyAllocConsole();
	GetPointer();

	PutDebugData();

}


void Debug::PutDebugData()
{
	
	if (dunConsole != nullptr && dun != nullptr)
	{
		*dunConsole << "Dungeon" << endline << endline;
		*dunConsole << "count : " << dun->cntDungeon << endline << endline;
		*dunConsole << "player.pos = ( " << dun->player.pos.x << " , " << dun->player.pos.y << " )" << endline;
		*dunConsole << "player.moveVector = ( " << dun->player.moveVector.x << " , " << dun->player.moveVector.y << " )" << endline << endline;

		Enemy *it;
		std::list<Enemy *>::iterator itr = dun->enemyList.begin();
		it = *(itr++);
		*dunConsole << "enemy1.pos = ( " << it->pos.x << " , " << it->pos.y << " )" << endline;
		*dunConsole << "enemy1.moveVector = ( " << it->moveVector.x << " , " << it->moveVector.y << " )" << endline << endline;
		it = *itr;
		*dunConsole << "enemy2.pos = ( " << it->pos.x << " , " << it->pos.y << " )" << endline;
		*dunConsole << "enemy2.moveVector = ( " << it->moveVector.x << " , " << it->moveVector.y << " )" << endline;



		*dunConsole << flip;
	}
}

void Debug::GetPointer()
{
	dun = nullptr;
	SceneMgr *pSceneMgr = pGame->pSceneMgr;
	if( !pSceneMgr->scene.empty() && pSceneMgr->scene.top()->thisScene == eScene_DUNGEON )
		dun = (Dungeon*)pSceneMgr->scene.top();
}


//==========================================
//         Control Allocating
//==========================================
void Debug::UpdateOutputFlag()
{
	if (GetKey(KEY_INPUT_LCONTROL) && GetKey(KEY_INPUT_F1) == 1)		fInConsole = false;
	else if (GetKey(KEY_INPUT_F1) == 1)									fInConsole = true;

	if (GetKey(KEY_INPUT_LCONTROL) && GetKey(KEY_INPUT_F5) == 1)		fDun = false;
	else if (GetKey(KEY_INPUT_F5) == 1)									fDun = true;
}

void Debug::MyAllocConsole()
{
	if (fDun && dunConsole == nullptr)
		dunConsole = new OutputConsole;
	else if (!fDun && dunConsole != nullptr)
		DEL(dunConsole);

	if (fInConsole && inCon == nullptr)
		inCon = new Console;
	else if (!fInConsole && inCon != nullptr)
		DEL(inCon);
}
