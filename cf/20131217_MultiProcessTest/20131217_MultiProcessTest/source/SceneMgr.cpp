#include <iostream>
#include "SceneMgr.h"

#include "Title.h"
#include "Dungeon.h"



SceneMgr::SceneMgr()
{
	while( !scene.empty() )
	{
		delete scene.top();
		scene.pop();
	}

	SetScene(eScene::eScene_TITLE, false);
}

SceneMgr::~SceneMgr()
{
	while( !scene.empty() )
	{
		delete scene.top();
		scene.pop();
	}
}


void SceneMgr::SetScene(eScene nextScene, bool fScenePop)
{
	this->fScenePop = fScenePop;
	this->nextScene = nextScene;
}


bool SceneMgr::Update()
{
	if( nextScene == eScene_NONE )
		; //VOID
	else
	{
		if( fScenePop )
		{
			delete scene.top();
			scene.pop();
		}

		switch( nextScene )
		{
			case eScene_TITLE:		scene.push(new Title(this));	break;
			case eScene_DUNGEON:	scene.push(new Dungeon(this));	break;
			default:				return false;
		}

		nextScene = eScene_NONE;
	}

	scene.top()->Process();

	return true;
}