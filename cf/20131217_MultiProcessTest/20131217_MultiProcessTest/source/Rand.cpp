#include <time.h>
#include <math.h>
#include "Rand.h"
#include "MT.h"

bool Rand::Initialize()
{
	mt::init_genrand((unsigned int)time(NULL));
	return true;
}

int Rand::GetRand(int min, int max)
{
	return (int)floor(min + (max - min + 1) * mt::genrand_real2());
}

double Rand::GetRand(double min, double max)
{
	return min + (max - min) * mt::genrand_real1();
}