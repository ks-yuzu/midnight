#include "Chip.h"
#include "DxLib.h"

//vector<Chip> Chip::chipDatabase;
int Chip::hImgChip;
int Chip::hImgObject;

Chip::Chip(){}
//Chip::Chip(bool canEnter)	//�J���p
//:fEnter(canEnter)
//{}

bool Chip::LoadChipData()
{
	hImgChip	= LoadGraph("database/image/map/chip/grass.png");
	hImgObject	= LoadGraph("database/image/map/object/tree.png");

	if( hImgChip == -1 || hImgObject == -1 )	return false;
	return true;
}

void Chip::SetEnterFlag(bool canEnter){ fEnter = canEnter; }
bool Chip::GetEnterFlag(){ return fEnter; }

void Chip::Draw(int x, int y)
{
	DrawGraph(x * 96, y * 96, hImgChip, true);

	if( !fEnter )
		DrawGraph(x * 96, y * 96, hImgObject, true);

}