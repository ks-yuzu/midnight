#pragma once

#include <string>
#include <sstream>
#include <Windows.h>

using namespace std;

#define endline "\n"
#define flip "!flip"

class OutputConsole
{
public:
	OutputConsole();
	~OutputConsole();
	
	HANDLE GetWritePipe(){ return writePipe; };

	void Print(string);

	template<class T> OutputConsole& operator<<(T val)
	{
		stringstream buf;
		buf << val << "|";
		Print(buf.str());

		return *this;
	}


private:
	static char consoleFile[128];

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	HANDLE hProcThread;

	HANDLE hParenProcess;							// 親プロセス(カレントプロセス)のハンドル
	HANDLE hChildProcess;							// 子プロセスのハンドル


	HANDLE readPipe, writePipe;

	bool Process();
	static unsigned int __stdcall LaunchThread(void *);
	unsigned int __stdcall TProcess();


};