#include "Game.h"

#include "DxLib.h"
#include "Input.h"
#include "Console.h"
#include "Fps.h"
#include "SceneMgr.h"
#include "Chip.h"
#include "Rand.h"

#include "LogOutput.h"
#include "Debug.h"

Game::Game()
{
	pInput = new InputMgr;
	pSceneMgr = new SceneMgr;
	pFps = new Fps;

	pDebug = new Debug;

}

Game::~Game()
{
	if (pInput != nullptr)		delete pInput;
	if (pSceneMgr != nullptr)	delete pSceneMgr;
	if (pFps != nullptr)		delete pFps;

	if (pDebug != nullptr)		delete pDebug;
}

bool Game::Initialize()
{

	int dispx = GetSystemMetrics(SM_CXSCREEN);												//モニタサイズ取得
	SetWindowInitPosition( dispx-980-30 , 30 ) ;											//ウィンドウ初期位置設定(ウィンドウサイズ960 , 調整20) -> -980

	SetGraphMode( 960 , 672 , 16 );															//ウィンドウサイズ設定

	SetMainWindowText( "20131217_設計テスト" );												//タイトル設定

	SetWindowUserCloseEnableFlag(FALSE);													//ペケポンガード 〜 WM_CLOSEメッセージとDestroyWindow関数を切り離す

	if( SetAlwaysRunFlag(TRUE) == -1 ) return false;										//非アクティブ時の動作可能設定
	
	if( ChangeWindowMode(TRUE) != DX_CHANGESCREEN_OK || DxLib_Init() == -1 ) return false;	//ウィンドウ化と初期化

	if( SetDrawScreen( DX_SCREEN_BACK ) == -1) return false;								//ダブルバッファリングに設定

	
	//	SetWindowPos(GetMainWindowHandle(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);			//常に最前面
	//	SetWindowPos(GetMainWindowHandle(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);		//常に最前面解除

	Rand::Initialize();																		//乱数のシード設定

	Log << "NoDoubleBuffering";
	Log << "Log Window" << endline << endline;												//ログウィンドウの設定とタイトル

	if( !Chip::LoadChipData() ) return false;

	return true;
}

void Game::Terminalize()
{
	DxLib_End();
	return;
}

bool Game::ProcessFrame()
{
	pDebug->DebugMgr(this);
	pSceneMgr->Update();
	return true;
}

bool Game::ConfirmExit()
{
	bool fExit = false;

	if(GetWindowUserCloseFlag(TRUE) == TRUE || pInput->GetKey(KEY_INPUT_F12))
		if ( (pInput->GetKey(KEY_INPUT_F11) && pInput->GetKey(KEY_INPUT_F12)
			|| MessageBox(GetMainWindowHandle() , "終了しますか？", "Confirm", MB_OKCANCEL) == IDOK) )
				fExit = true;

	return fExit;
}