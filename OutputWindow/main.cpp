#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

#include "PrintMgr.h"

int main(int argc, char **argv)
{
	PrintMgr pm;
	return pm.Process(argc, argv);
}
