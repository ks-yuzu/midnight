#include <fstream>
#include <windows.h>

const int MAX_WIDTH  = 80;
const int MAX_HEIGHT  = 25;
const COORD base = { 0, 0 };
const COORD dbmax = { MAX_WIDTH, MAX_HEIGHT };
const COORD ndbmax = { MAX_WIDTH, MAX_HEIGHT * 100 };
const SMALL_RECT sr = { 0, 0, (MAX_WIDTH - 1), (MAX_HEIGHT - 1) };
const HANDLE hScreen[2] = {	CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL),
							CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL) };

class PrintMgr
{
public:
	PrintMgr();
	virtual ~PrintMgr();

	bool Init(int, char**);
	int Process(int, char**);
	void ScreenFlip();

private:
	//VARIABLES
	bool fDoubleBuffering = true;
	int currentScreen;
	DWORD dummy;
	HWND hThisWnd;

	//OPERATIONS
	void DisableDoubleBuffering();
	void ClearOffBuffer();
	void SetTextColor(std::string);
};
