#include <string>
#include <sstream>
#include <algorithm>
#include "PrintMgr.h"
#include "../Game/source/output_data.h"

PrintMgr::PrintMgr()
: currentScreen(0)
{}


PrintMgr::~PrintMgr()
{
	for each (HANDLE hScreen in hScreen)
		CloseHandle(hScreen);
}


bool PrintMgr::Init(int argc, char **argv)
{
	if( argc <= 1 )	return false;
	
	if     ( std::string(argv[1]) == "/D" )	fDoubleBuffering = true;
	else if( std::string(argv[1]) == "/L" )	fDoubleBuffering = false;
	else									return false;


	COORD max = fDoubleBuffering ? dbmax : ndbmax;

	for each (HANDLE hScreen in hScreen)
	{
		SetConsoleScreenBufferSize(hScreen, max);
		SetConsoleWindowInfo(hScreen, TRUE, &sr);
	}

	SetConsoleActiveScreenBuffer(hScreen[0]);
	SetConsoleCursorPosition(hScreen[0], base);

	return true;
}


int PrintMgr::Process(int argc, char **argv)
{
	std::ifstream ifs(stdin);

	if( Init(argc, argv) == false ) return -1;

	while( 1 )
	{
		std::string str;
		std::getline(ifs, str, '|');

		if( fDoubleBuffering && str == Command::flip )
		{
			ScreenFlip();
			ClearOffBuffer();
			SetConsoleCursorPosition(hScreen[currentScreen], base);
		}
		else
			WriteConsole(hScreen[currentScreen], str.c_str(), strlen(str.c_str()), &dummy, NULL);
	}

	return 0;
}


void PrintMgr::ScreenFlip()
{
	SetConsoleActiveScreenBuffer(hScreen[currentScreen]);
	currentScreen = (currentScreen + 1) % 2;
	SetConsoleCursorPosition(hScreen[currentScreen], base);
}


void PrintMgr::ClearOffBuffer()
{
	static const char voidStr[MAX_WIDTH * MAX_HEIGHT + 1] = "";
	SetConsoleCursorPosition(hScreen[currentScreen], base);
	WriteConsole(hScreen[currentScreen], voidStr, static_cast<DWORD>(MAX_WIDTH * MAX_HEIGHT), &dummy, NULL);
}

/*
void PrintMgr::DisableDoubleBuffering()
{
	COORD max = { MAX_WIDTH, MAX_HEIGHT *  15 };
	SetConsoleScreenBufferSize(hScreen[0], max);

	SetConsoleActiveScreenBuffer(hScreen[0]);
	SetConsoleCursorPosition(hScreen[0], base);

	fDoubleBuffering = false;
}
*/