#include <algorithm>
#include "DxLib.h"
#include "input.h"
#include "key_id.h"


InputMgr::InputMgr()
{
	FillZero();
}


bool InputMgr::Update()
{
	char buf[256];

	if( GetHitKeyStateAll(buf) == -1 )
	{
		FillZero();
		return false;	// 更新失敗したら0にして抜ける
	}

	for( int i = 0; i < 256; i++ )
	{
		if( buf[i] == 1 )	key[i]++;
		else				key[i] = 0;
	}

	return true;
}


InputMgr& InputMgr::GetInstance()
{
	static InputMgr input;
	return input;
}


unsigned int InputMgr::GetKeyState(int keyId)
{
	return	key[keyId];
}

bool InputMgr::IsUp()		{ return !!key[KEY_INPUT_UP];		}
bool InputMgr::IsDown()		{ return !!key[KEY_INPUT_DOWN];		}
bool InputMgr::IsRight()	{ return !!key[KEY_INPUT_RIGHT];	}
bool InputMgr::IsLeft()		{ return !!key[KEY_INPUT_LEFT];		}
bool InputMgr::IsEnter()	{ return !!key[KEY_INPUT_X] || key[KEY_INPUT_RETURN]; }
bool InputMgr::IsCancel()	{ return !!key[KEY_INPUT_Z] || key[KEY_INPUT_ESCAPE]; }