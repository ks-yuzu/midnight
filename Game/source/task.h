#pragma once

class Task
{
	public:
		virtual ~Task(){};
		virtual bool Initialize()	{ return true; }
		virtual bool Terminalize()	{ return true; }
		virtual bool Update() = 0;
		virtual void Draw()			{ return; }
};