#include <random>

class MyRand
{
public:
	virtual int operator()(int, int) = 0;
	virtual double operator()(double, double) = 0;

protected:
	std::mt19937_64 mt;
	MyRand();
};


//========================================
//               一様分布
//========================================
class MyUniformRand : public MyRand
{
public:
	int operator()(int, int) override;
	double operator()(double, double) override;
	static MyUniformRand& GetInstance();
};


inline int MyUniformRand::operator()(int min, int max)
{
	std::uniform_int_distribution<int> dist(min, max);
	return dist(mt);
}


inline double MyUniformRand::operator()(double min, double max)
{
	std::uniform_real_distribution<double> dist(min, max);
	return dist(mt);
}



//========================================
//               正規分布
//========================================
class MyNormalRand : public MyRand
{
public:
	int operator()(int, int) override;
	double operator()(double, double) override;
	static MyNormalRand& GetInstance();
};


inline int MyNormalRand::operator()(int min, int max)
{
	std::normal_distribution<> dist(min, max);
	return (int)dist(mt);
}


inline double MyNormalRand::operator()(double min, double max)
{
	std::normal_distribution<> dist(min, max);
	return dist(mt);
}
