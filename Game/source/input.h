#pragma once

#include "task.h"

class InputMgr : public Task
{
	public:
	//OPERATIONS
		bool Update() override;

	//ACCESS
		static InputMgr& GetInstance();
		unsigned int GetKeyState(int);

	//INQUIRY
		bool IsUp();
		bool IsDown();
		bool IsRight();
		bool IsLeft();
		bool IsEnter();
		bool IsCancel();

	private:
	//LIFECYCLE
		InputMgr();

	//OPERATIONS
		void FillZero();

	//VARIABLES
		unsigned int key[256];	//押下キーフレーム数
};


inline void InputMgr::FillZero()
{
	for each (int keyItem in key)
		keyItem = 0;
}

