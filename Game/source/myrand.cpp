#include "myrand.h"

MyRand::MyRand()
{
	std::random_device rnd;
	mt.seed( rnd() );
}


MyUniformRand& MyUniformRand::GetInstance()
{
	static MyUniformRand rand;
	return rand;
}


MyNormalRand& MyNormalRand::GetInstance()
{
	static MyNormalRand rand;
	return rand;
}
