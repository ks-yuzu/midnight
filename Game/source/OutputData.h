#pragma once

#include <string>

const std::string endline					= "\n";
const std::string commandFlip				= "!flip";
const std::string commandExit				= "!exit";
const std::string commandNoDoubleBuffering	= "!NDB";
const std::string commandSetTextBlue		= "!TextColorBlue";
const std::string commandSetTextGreen		= "!TextColorGreen";
const std::string commandSetTextRed			= "!TextColorRed";
const std::string commandSetTextCyan		= "!TextColorCyan";
const std::string commandSetTextPurple		= "!TextColorPurple";
const std::string commandSetTextYellow		= "!TextColorYellow";
const std::string commandSetTextWhite		= "!TextColorWhite";