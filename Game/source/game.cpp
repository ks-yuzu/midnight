#include "DxLib.h"
#include "game.h"
#include "define.h"
#include "log_window.h"


Game::Game()
:	fps(FpsControl::GetInstance()),
	input(InputMgr::GetInstance()),
	count(0)
{}


bool Game::Initialize()
{
	//=================================================
	//                 WINDOW SETTING 
	//=================================================
	int dispWidth = GetSystemMetrics(SM_CXSCREEN);											//モニタサイズ取得
	SetWindowInitPosition( dispWidth - windowSizeX - posXOffset , posYOffset ) ;			//ウィンドウ初期位置設定( offset 30pixel )
	SetGraphMode( windowSizeX , windowSizeY , bitDepth );									//ウィンドウサイズ設定
	SetCreateGraphColorBitDepth( bitDepth );												//グラフィックのビット深度
	SetMainWindowText( (title + "  FPS:     ").c_str() );									//タイトル設定
//	SetWindowIconHandle();

	SetWindowUserCloseEnableFlag(FALSE);													//WM_CLOSEメッセージとDestroyWindow関数を切り離す -> GetWindowUserCloseFlag()

	if( SetAlwaysRunFlag(TRUE) == -1 )					return false;						//非アクティブ時の動作可能設定
	if( ChangeWindowMode(TRUE) != DX_CHANGESCREEN_OK )	return false;						//ウィンドウ化
	if( DxLib_Init() == -1 )							return false;						//初期化
	if( SetDrawScreen( DX_SCREEN_BACK ) == -1 )			return false;						//ダブルバッファリング設定
	
//	SetWindowPos(GetMainWindowHandle(), HWND_TOPMOST  , 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);			//常に最前面
//	SetWindowPos(GetMainWindowHandle(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);			//常に最前面解除


	//=================================================
	//                 SYSTEM SETTING 
	//=================================================
	Log::cout.Init();
	Log::cout << "Log Window" << Command::endline << Command::endline;											//ログウィンドウの設定とタイトル

//	if( !Chip::LoadChipData() )						return false;

	return true;
}


bool Game::Terminalize()
{
	DxLib_End();
	return true;
}


bool Game::Update()
{
	//debug
	//scene
	return true;
}


bool Game::ConfirmExit()
{
	//終了動作が行われていない
	if( !GetWindowUserCloseFlag(TRUE) && !input.GetKeyState(KEY_INPUT_F12) )
		return false;	

	//即終了キー
	if( input.GetKeyState(KEY_INPUT_F11) && input.GetKeyState(KEY_INPUT_F12) )
		return true;

	return ( MessageBox(GetMainWindowHandle() , "終了しますか？", "Confirm", MB_OKCANCEL) == IDOK );
}
