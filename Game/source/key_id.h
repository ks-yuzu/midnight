#pragma once

#ifndef __DXDIRECTX_H__
	#include "DxDirectX.h"

#endif

#ifndef __DXLIB
	// キー定義
	#define KEY_INPUT_BACK								D_DIK_BACK			// バックスペースキー
	#define KEY_INPUT_TAB								D_DIK_TAB			// タブキー
	#define KEY_INPUT_RETURN							D_DIK_RETURN		// エンターキー

	#define KEY_INPUT_LSHIFT							D_DIK_LSHIFT		// 左シフトキー
	#define KEY_INPUT_RSHIFT							D_DIK_RSHIFT		// 右シフトキー
	#define KEY_INPUT_LCONTROL							D_DIK_LCONTROL		// 左コントロールキー
	#define KEY_INPUT_RCONTROL							D_DIK_RCONTROL		// 右コントロールキー
	#define KEY_INPUT_ESCAPE							D_DIK_ESCAPE		// エスケープキー
	#define KEY_INPUT_SPACE								D_DIK_SPACE			// スペースキー
	#define KEY_INPUT_PGUP								D_DIK_PGUP			// ＰａｇｅＵＰキー
	#define KEY_INPUT_PGDN								D_DIK_PGDN			// ＰａｇｅＤｏｗｎキー
	#define KEY_INPUT_END								D_DIK_END			// エンドキー
	#define KEY_INPUT_HOME								D_DIK_HOME			// ホームキー
	#define KEY_INPUT_LEFT								D_DIK_LEFT			// 左キー
	#define KEY_INPUT_UP								D_DIK_UP			// 上キー
	#define KEY_INPUT_RIGHT								D_DIK_RIGHT			// 右キー
	#define KEY_INPUT_DOWN								D_DIK_DOWN			// 下キー
	#define KEY_INPUT_INSERT							D_DIK_INSERT		// インサートキー
	#define KEY_INPUT_DELETE							D_DIK_DELETE		// デリートキー

	#define KEY_INPUT_MINUS								D_DIK_MINUS			// −キー
	#define KEY_INPUT_YEN								D_DIK_YEN			// ￥キー
	#define KEY_INPUT_PREVTRACK							D_DIK_PREVTRACK		// ＾キー
	#define KEY_INPUT_PERIOD							D_DIK_PERIOD		// ．キー
	#define KEY_INPUT_SLASH								D_DIK_SLASH			// ／キー
	#define KEY_INPUT_LALT								D_DIK_LALT			// 左ＡＬＴキー
	#define KEY_INPUT_RALT								D_DIK_RALT			// 右ＡＬＴキー
	#define KEY_INPUT_SCROLL							D_DIK_SCROLL		// ScrollLockキー
	#define KEY_INPUT_SEMICOLON							D_DIK_SEMICOLON		// ；キー
	#define KEY_INPUT_COLON								D_DIK_COLON			// ：キー
	#define KEY_INPUT_LBRACKET							D_DIK_LBRACKET		// ［キー
	#define KEY_INPUT_RBRACKET							D_DIK_RBRACKET		// ］キー
	#define KEY_INPUT_AT								D_DIK_AT			// ＠キー
	#define KEY_INPUT_BACKSLASH							D_DIK_BACKSLASH		// ＼キー
	#define KEY_INPUT_COMMA								D_DIK_COMMA			// ，キー
	#define KEY_INPUT_KANJI								D_DIK_KANJI			// 漢字キー
	#define KEY_INPUT_CONVERT							D_DIK_CONVERT		// 変換キー
	#define KEY_INPUT_NOCONVERT							D_DIK_NOCONVERT		// 無変換キー
	#define KEY_INPUT_KANA								D_DIK_KANA			// カナキー
	#define KEY_INPUT_APPS								D_DIK_APPS			// アプリケーションメニューキー
	#define KEY_INPUT_CAPSLOCK							D_DIK_CAPSLOCK		// CaspLockキー
	#define KEY_INPUT_SYSRQ								D_DIK_SYSRQ			// PrintScreenキー
	#define KEY_INPUT_PAUSE								D_DIK_PAUSE			// PauseBreakキー
	#define KEY_INPUT_LWIN								D_DIK_LWIN			// 左Ｗｉｎキー
	#define KEY_INPUT_RWIN								D_DIK_RWIN			// 右Ｗｉｎキー

	#define KEY_INPUT_NUMLOCK							D_DIK_NUMLOCK		// テンキー０
	#define KEY_INPUT_NUMPAD0							D_DIK_NUMPAD0		// テンキー０
	#define KEY_INPUT_NUMPAD1							D_DIK_NUMPAD1		// テンキー１
	#define KEY_INPUT_NUMPAD2							D_DIK_NUMPAD2		// テンキー２
	#define KEY_INPUT_NUMPAD3							D_DIK_NUMPAD3		// テンキー３
	#define KEY_INPUT_NUMPAD4							D_DIK_NUMPAD4		// テンキー４
	#define KEY_INPUT_NUMPAD5							D_DIK_NUMPAD5		// テンキー５
	#define KEY_INPUT_NUMPAD6							D_DIK_NUMPAD6		// テンキー６
	#define KEY_INPUT_NUMPAD7							D_DIK_NUMPAD7		// テンキー７
	#define KEY_INPUT_NUMPAD8							D_DIK_NUMPAD8		// テンキー８
	#define KEY_INPUT_NUMPAD9							D_DIK_NUMPAD9		// テンキー９
	#define KEY_INPUT_MULTIPLY							D_DIK_MULTIPLY		// テンキー＊キー
	#define KEY_INPUT_ADD								D_DIK_ADD			// テンキー＋キー
	#define KEY_INPUT_SUBTRACT							D_DIK_SUBTRACT		// テンキー−キー
	#define KEY_INPUT_DECIMAL							D_DIK_DECIMAL		// テンキー．キー
	#define KEY_INPUT_DIVIDE							D_DIK_DIVIDE		// テンキー／キー
	#define KEY_INPUT_NUMPADENTER						D_DIK_NUMPADENTER	// テンキーのエンターキー

	#define KEY_INPUT_F1								D_DIK_F1			// Ｆ１キー
	#define KEY_INPUT_F2								D_DIK_F2			// Ｆ２キー
	#define KEY_INPUT_F3								D_DIK_F3			// Ｆ３キー
	#define KEY_INPUT_F4								D_DIK_F4			// Ｆ４キー
	#define KEY_INPUT_F5								D_DIK_F5			// Ｆ５キー
	#define KEY_INPUT_F6								D_DIK_F6			// Ｆ６キー
	#define KEY_INPUT_F7								D_DIK_F7			// Ｆ７キー
	#define KEY_INPUT_F8								D_DIK_F8			// Ｆ８キー
	#define KEY_INPUT_F9								D_DIK_F9			// Ｆ９キー
	#define KEY_INPUT_F10								D_DIK_F10			// Ｆ１０キー
	#define KEY_INPUT_F11								D_DIK_F11			// Ｆ１１キー
	#define KEY_INPUT_F12								D_DIK_F12			// Ｆ１２キー

	#define KEY_INPUT_A									D_DIK_A			// Ａキー
	#define KEY_INPUT_B									D_DIK_B			// Ｂキー
	#define KEY_INPUT_C									D_DIK_C			// Ｃキー
	#define KEY_INPUT_D									D_DIK_D			// Ｄキー
	#define KEY_INPUT_E									D_DIK_E			// Ｅキー
	#define KEY_INPUT_F									D_DIK_F			// Ｆキー
	#define KEY_INPUT_G									D_DIK_G			// Ｇキー
	#define KEY_INPUT_H									D_DIK_H			// Ｈキー
	#define KEY_INPUT_I									D_DIK_I			// Ｉキー
	#define KEY_INPUT_J									D_DIK_J			// Ｊキー
	#define KEY_INPUT_K									D_DIK_K			// Ｋキー
	#define KEY_INPUT_L									D_DIK_L			// Ｌキー
	#define KEY_INPUT_M									D_DIK_M			// Ｍキー
	#define KEY_INPUT_N									D_DIK_N			// Ｎキー
	#define KEY_INPUT_O									D_DIK_O			// Ｏキー
	#define KEY_INPUT_P									D_DIK_P			// Ｐキー
	#define KEY_INPUT_Q									D_DIK_Q			// Ｑキー
	#define KEY_INPUT_R									D_DIK_R			// Ｒキー
	#define KEY_INPUT_S									D_DIK_S			// Ｓキー
	#define KEY_INPUT_T									D_DIK_T			// Ｔキー
	#define KEY_INPUT_U									D_DIK_U			// Ｕキー
	#define KEY_INPUT_V									D_DIK_V			// Ｖキー
	#define KEY_INPUT_W									D_DIK_W			// Ｗキー
	#define KEY_INPUT_X									D_DIK_X			// Ｘキー
	#define KEY_INPUT_Y									D_DIK_Y			// Ｙキー
	#define KEY_INPUT_Z									D_DIK_Z			// Ｚキー

	#define KEY_INPUT_0 								D_DIK_0			// ０キー
	#define KEY_INPUT_1									D_DIK_1			// １キー
	#define KEY_INPUT_2									D_DIK_2			// ２キー
	#define KEY_INPUT_3									D_DIK_3			// ３キー
	#define KEY_INPUT_4									D_DIK_4			// ４キー
	#define KEY_INPUT_5									D_DIK_5			// ５キー
	#define KEY_INPUT_6									D_DIK_6			// ６キー
	#define KEY_INPUT_7									D_DIK_7			// ７キー
	#define KEY_INPUT_8									D_DIK_8			// ８キー
	#define KEY_INPUT_9									D_DIK_9			// ９キー

#endif