#include <sstream>
#include <iomanip>
#include <memory>
#include "DxLib.h"
#include "fps.h"
#include "define.h"


FpsControl::FpsControl()
:start(GetNowCount()), count(0), fps(0)
{}


FpsControl::FpsControl(FpsControl& aFps)
:start(aFps.start), count(aFps.count), fps(aFps.fps)
{}


FpsControl& FpsControl::GetInstance()
{
	static FpsControl fps;
	return fps;
}


bool FpsControl::Update()
{
	if( count == MAX_COUNT )
	{
		CalcFps();
		Draw();
		count = 0;
		start = GetNowCount();
	}

	count++;
	return true;
}


void FpsControl::CalcFps()
{
	int now = GetNowCount();
	fps = 1000. * MAX_COUNT / ( now - start );
}


void FpsControl::Draw()
{
	std::ostringstream os;
	os << title << "  FPS:" << std::fixed << std::setprecision(2) << fps;
	SetMainWindowText( os.str().c_str() );
}


void FpsControl::Wait()
{
	int passTime = GetNowCount() - start;
	int waitTime = count * 1000 / STANDERD_FPS - passTime;

	if( waitTime > 0 )
		Sleep(waitTime);
}



