#include "DxLib.h"
#include "game.h"


#include "myrand.h"
#include "disp_window.h"
#include "log_window.h"

namespace {

class GameFrame : public Game
{
public:

	int GameMain()			//ゲーム内容以外のシステム処理（カウンタ・デバッグ関連etc）
	{
		if( !Initialize() ) return -1;

		while( !ProcessMessage() && !ClearDrawScreen() )
		{
			fps.Update();
			input.Update();

			if( ConfirmExit() )     break;	//終了確認 → 戻り値true

			//メイン処理
			//if( !ProcessFrame() ) break;	//異常発生 → 戻り値false



			// TEST
			if( MyUniformRand()(1,10) == 5 )
				Log::cout << "[ Generate UniformRand ]  " << MyUniformRand()(1,10) << Command::endline;
//			*DispWindow::GetDispWindow(0) << MyUniformRand()(1,10) << "  " << MyUniformRand()(1,10) << "  " << Command::endline;




			ScreenFlip();
			DispWindow::AllWindowFlip();

			fps.Wait();						//FPS調整用のウェイト
		}

		Terminalize();
		return 0;
	}

};
} // unnamed namespace




int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GameFrame gameObj;
	return gameObj.GameMain();
}