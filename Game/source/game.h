#pragma once

#include "task.h"
#include "fps.h"
#include "input.h"

class Game : public Task
{
	protected:
	//VARIALBLE
		FpsControl& fps;
		InputMgr& input;

		unsigned int count;

	//LIFECYCLE
		Game();
		virtual ~Game(){};
		bool Initialize() override;
		bool Terminalize() override;
		bool Update() override;
	
	//OPERATIONS
		bool ConfirmExit();

};