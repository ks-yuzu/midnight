#include "task.h"

class FpsControl : public Task
{
	public:
	//ACCESS
		static FpsControl& GetInstance();

	//OPERRATIONS
		bool Update() override;
		void Draw() override;

		void CalcFps();
		void Wait();


	private:
	//CONSTANTS
		const int MAX_COUNT = 60;
		const int STANDERD_FPS = 60;

	//LIFECYCLE
		FpsControl();
		FpsControl(FpsControl&);

	//VARIABLES
		int count, start;
		double fps;
};
