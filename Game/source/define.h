#pragma once

#include <string>

static const std::string title = "20141204_Refactoring";

static const int windowSizeX	= 960;
static const int windowSizeY	= 672;
static const int bitDepth		= 32;
static const int posXOffset		= 30;
static const int posYOffset		= 20;