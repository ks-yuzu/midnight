#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <Windows.h>
#include "OutputData.h"

class OutputWindow
{
public:
	//LIFECYCLE
	virtual ~OutputWindow();

	//OPRATOR
	template<class T>
	OutputWindow& operator<<(T val);

	//OPERATIONS
	void Print(std::string);
	static void Update();

	//ACCESS
	static std::shared_ptr<OutputWindow> GetOutputWindow(int idx);


private:
	static const std::string consoleFile;
	static std::vector<std::shared_ptr<OutputWindow> > outputs;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	HANDLE hProcThread;
	HANDLE hParenProcess;							// 親プロセス(カレントプロセス)のハンドル
	HANDLE hChildProcess;							// 子プロセスのハンドル
	HANDLE readPipe, writePipe;

	int windowNum;
	std::string titleName;

	//LIFECYCLE
	OutputWindow(){};
	OutputWindow(int);
	void Init();
	void InitStartupInfo(STARTUPINFO&);

	//OPERATIONS
	bool StartProcess();
	static unsigned int __stdcall LaunchThread(void *);
	unsigned int __stdcall TProcess();

};


inline std::shared_ptr<OutputWindow> OutputWindow::GetOutputWindow(int idx)
{
	if( !outputs[idx] )
	{
		outputs[idx] = std::shared_ptr<OutputWindow>(new OutputWindow(idx));
		outputs[idx]->Init();
	}

	return outputs[idx];
}


template<class T>
inline OutputWindow& OutputWindow::operator<<(T val)
{
	std::stringstream buf;
	buf << val << "|";
	Print( buf.str() );

	return *this;
}

inline void OutputWindow::Update()
{
	for each (std::shared_ptr<OutputWindow> ow in outputs)
	{
		if( !ow ) continue;
		*ow << commandFlip;
	}
}