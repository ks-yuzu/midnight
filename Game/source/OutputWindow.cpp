#define  _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <cassert>
#include <process.h>
#include <tchar.h>
#include "OutputWindow.h"
#include "OutputData.h"


const std::string OutputWindow::consoleFile = "OutputWindow.exe";
std::vector<std::shared_ptr<OutputWindow> > OutputWindow::outputs(6);


OutputWindow::OutputWindow(int wn)
:hParenProcess( GetCurrentProcess() ),
 hChildProcess(NULL),
 readPipe(nullptr),
 writePipe(nullptr),
 windowNum(wn),
 titleName("OutputWindow No." + std::to_string(wn+1))
{}


void OutputWindow::Init()
{
	HANDLE tmpReadPipe;

	// パイプを作成（両ハンドルとも子プロセスへ継承不可）
	assert( CreatePipe(&tmpReadPipe, &writePipe, NULL, 0) );

	// 読込ハンドルを複製（子プロセスへ継承可能な権限の読込ハンドルを作成）
	assert( DuplicateHandle( hParenProcess, tmpReadPipe, hParenProcess, &readPipe, 0, TRUE, DUPLICATE_SAME_ACCESS) );

	// 複製元のハンドルは使わないのでクローズ
	assert( CloseHandle(tmpReadPipe) );

	InitStartupInfo(si);
	StartProcess();
}


void OutputWindow::InitStartupInfo(STARTUPINFO& si)
{
	const WORD colors[6] = {
		FOREGROUND_GREEN | FOREGROUND_INTENSITY,						//Green
		FOREGROUND_BLUE  | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Purple
		FOREGROUND_BLUE  | FOREGROUND_GREEN |  FOREGROUND_INTENSITY,	//Cyan
		FOREGROUND_GREEN | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Blue
		FOREGROUND_BLUE  | FOREGROUND_INTENSITY,						//Blue
		FOREGROUND_RED   | FOREGROUND_INTENSITY,						//Red
	};

	ZeroMemory( &si, sizeof(STARTUPINFO) );

	si.cb				= sizeof(STARTUPINFO);
	si.dwFlags			= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW | STARTF_USEPOSITION | STARTF_USEFILLATTRIBUTE;
	si.wShowWindow		= SW_SHOWNOACTIVATE;
	si.lpTitle			= const_cast<LPSTR>( titleName.c_str() );
	si.dwX				= 10 + 650 * (windowNum / 2);
	si.dwY				= 20 + 494 * (windowNum % 2);
	si.dwFillAttribute	= colors[windowNum];
	si.hStdInput		= readPipe;
	si.hStdOutput		= GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdError		= GetStdHandle(STD_ERROR_HANDLE);

	// 確保した標準出力のチェック
	assert( si.hStdOutput != INVALID_HANDLE_VALUE );
	assert( si.hStdError != INVALID_HANDLE_VALUE );
}


bool OutputWindow::StartProcess()
{
	hProcThread = (HANDLE)_beginthreadex(NULL, 0, LaunchThread, this, 0, NULL);
//	hProcThread = (HANDLE)_beginthreadex(NULL, 0, this->TProcess, this, 0, NULL);

	if( hProcThread != nullptr )
		CloseHandle( hProcThread );

	return true;
}

OutputWindow::~OutputWindow()
{
	*this << commandExit;
	CloseHandle(writePipe);
	CloseHandle(hChildProcess);
//	CloseHandle(hProcThread); 
}


void OutputWindow::Print(std::string str)
{
	DWORD dummy;
	WriteFile(writePipe, str.c_str(), strlen(str.c_str()), &dummy, NULL);
}



//=======================================
//           子プロセス側
//=======================================

// staticでないメンバ関数をマルチスレッドにするために、この関数をかませる
unsigned int __stdcall OutputWindow::LaunchThread(void *ptr)
{
	return reinterpret_cast<OutputWindow*>(ptr)->TProcess();
}


unsigned int __stdcall OutputWindow::TProcess()
{
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	assert( CreateProcess(NULL, (LPSTR)consoleFile.c_str(), NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi) );

	// Set child process handle to cause threads to exit.
	hChildProcess = pi.hProcess;

	// Close any unnecessary handles.
	CloseHandle(pi.hThread);

	// Close pipe handles (do not continue to modify the parent)
	// Need to make sure that no handles to the write end of the output pipe are maintained in this process
	// or else the pipe will not close when the child process exits
	// and the ReadFile will hang.
	CloseHandle(readPipe);
	readPipe = nullptr;

	return 0;

}