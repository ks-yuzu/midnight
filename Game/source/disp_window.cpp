#include <cassert>
#include "disp_window.h"


std::vector<std::shared_ptr<DispWindow> > DispWindow::outputs(6);

DispWindow::DispWindow(int wn)
:windowNum(wn+1),
OutputWindow(
	10 + 650 * (windowNum / 2),
	20 + 494 * (windowNum % 2),
	"OutputWindow No." + std::to_string(windowNum),
	dispConsole,
	windowNum
)
{}
